# Martin Pitt <mpitt@redhat.com>, 2019. #zanata
# Mike FABIAN <mfabian@redhat.com>, 2020.
# Matej Marusak <marusak.matej@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE_VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-31 02:51+0000\n"
"PO-Revision-Date: 2021-12-28 21:16+0000\n"
"Last-Translator: Ettore Atalan <atalanttore@googlemail.com>\n"
"Language-Team: German <https://translate.fedoraproject.org/projects/cockpit-"
"podman/main/de/>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1\n"
"X-Generator: Weblate 4.10.1\n"

#: src/Images.jsx:78
msgid "$0 container"
msgid_plural "$0 containers"
msgstr[0] "$0 Container"
msgstr[1] "$0 Container"

#: src/Images.jsx:253
msgid "$0 image total, $1"
msgid_plural "$0 images total, $1"
msgstr[0] "$0 Abbild insgesamt, $1"
msgstr[1] "$0 Abbilder insgesamt, $1"

#: src/Images.jsx:257
msgid "$0 unused image, $1"
msgid_plural "$0 unused images, $1"
msgstr[0] "$0 ungenutztes Abbild, $1"
msgstr[1] "$0 ungenutzte Abbilder, $1"

#: src/ImageRunModal.jsx:1012
msgid "Add port mapping"
msgstr "Port-Zuordnung hinzufügen"

#: src/ImageRunModal.jsx:1031
msgid "Add variable"
msgstr "Variable hinzufügen"

#: src/ImageRunModal.jsx:1021
msgid "Add volume"
msgstr "Volumen hinzufügen"

#: src/ContainerHeader.jsx:49 src/Containers.jsx:535 src/ImageRunModal.jsx:780
msgid "All"
msgstr "Alle"

#: src/ImageSearchModal.jsx:167
msgid "All registries"
msgstr "Alle Registries"

#: src/ImageRunModal.jsx:978
msgid "Always"
msgstr "Immer"

#: src/PodActions.jsx:178
msgid "An error occurred"
msgstr "Ein Fehler ist aufgetreten"

#: src/ContainerCommitModal.jsx:116
msgid "Author"
msgstr "Autor"

#: src/app.jsx:587
msgid "Automatically start podman on boot"
msgstr "Podman automatisch beim Hochfahren starten"

#: src/Containers.jsx:427
msgid "CPU"
msgstr "CPU"

#: src/ImageRunModal.jsx:934
msgid "CPU Shares help"
msgstr ""

#: src/ImageRunModal.jsx:932
msgid "CPU shares"
msgstr "CPU-Shares"

#: src/ImageRunModal.jsx:936
msgid ""
"CPU shares determine the priority of running containers. Default priority is "
"1024. A higher number prioritizes this container. A lower number decreases "
"priority."
msgstr ""

#: src/ContainerCheckpointModal.jsx:38 src/ForceRemoveModal.jsx:21
#: src/ContainerRestoreModal.jsx:39 src/ContainerCommitModal.jsx:168
#: src/ContainerDeleteModal.jsx:15 src/ImageSearchModal.jsx:231
#: src/PodActions.jsx:174 src/PruneUnusedImagesModal.jsx:103
#: src/ImageRunModal.jsx:1059 src/ImageDeleteModal.jsx:62
msgid "Cancel"
msgstr "Abbruch"

#: src/ContainerCheckpointModal.jsx:34 src/Containers.jsx:215
msgid "Checkpoint"
msgstr "Kontrollpunkt"

#: src/ContainerCheckpointModal.jsx:29
msgid "Checkpoint container $0"
msgstr "Kontrollpunkt für Container $0"

#: src/ContainerCommitModal.jsx:123 src/ContainerDetails.jsx:60
#: src/ImageDetails.jsx:15 src/ImageRunModal.jsx:889
msgid "Command"
msgstr "Befehl"

#: src/Containers.jsx:243 src/ContainerCommitModal.jsx:155
msgid "Commit"
msgstr "Committen"

#: src/ContainerCommitModal.jsx:146
msgid "Commit container"
msgstr ""

#: src/util.js:8
#, fuzzy
#| msgid "configured"
msgid "Configured"
msgstr "konfiguriert"

#: src/Containers.jsx:402
msgid "Console"
msgstr "Konsole"

#: src/Containers.jsx:425
msgid "Container"
msgstr "Container"

#: src/ImageRunModal.jsx:459
msgid "Container failed to be created"
msgstr "Container konnte nicht erstellt werden"

#: src/ImageRunModal.jsx:442
msgid "Container failed to be started"
msgstr "Container konnte nicht erstellt werden"

#: src/Containers.jsx:288
msgid "Container is currently running."
msgstr "Container läuft gerade."

#: src/ContainerTerminal.jsx:262
msgid "Container is not running"
msgstr "Container läuft nicht"

#: src/ImageRunModal.jsx:818
msgid "Container name"
msgstr "Container-Name"

#: src/ImageRunModal.jsx:180
msgid "Container path"
msgstr "Container-Pfad"

#: src/ImageRunModal.jsx:91
msgid "Container port"
msgstr "Container-Port"

#: src/Containers.jsx:566 src/Containers.jsx:573 src/Containers.jsx:601
msgid "Containers"
msgstr "Container"

#: src/ImageRunModal.jsx:1056
msgid "Create"
msgstr "Erstellen"

#: src/ContainerCommitModal.jsx:147
msgid "Create a new image based on the current state of the $0 container."
msgstr ""
"Ein neues Abbild auf der Grundlage des aktuellen Zustands des Containers $0 "
"erstellen."

#: src/Containers.jsx:544 src/Images.jsx:388 src/ImageRunModal.jsx:1052
msgid "Create container"
msgstr "Container erstellen"

#: src/ImageRunModal.jsx:1052
#, fuzzy
#| msgid "Create container"
msgid "Create container in $0"
msgstr "Container erstellen"

#: src/Containers.jsx:620
#, fuzzy
#| msgid "Create container"
msgid "Create container in pod"
msgstr "Container erstellen"

#: src/util.js:8 src/util.js:11 src/ContainerDetails.jsx:84 src/Images.jsx:163
msgid "Created"
msgstr "Erstellt"

#: src/ImageRunModal.jsx:824
msgid "Default with single selectable"
msgstr ""

#: src/Containers.jsx:251 src/ContainerDeleteModal.jsx:14 src/Images.jsx:401
#: src/PodActions.jsx:151 src/PodActions.jsx:173
msgid "Delete"
msgstr "Löschen"

#: src/ImageDeleteModal.jsx:56
msgid "Delete $0"
msgstr "$0 löschen"

#: src/ImageDeleteModal.jsx:60
msgid "Delete tagged images"
msgstr "Markierte Image löschen"

#: src/PruneUnusedImagesModal.jsx:28
msgid "Delete unused system images:"
msgstr "Nicht verwendete Systemabbilder löschen:"

#: src/PruneUnusedImagesModal.jsx:28
#, fuzzy
#| msgid "Delete unused system images:"
msgid "Delete unused user images:"
msgstr "Nicht verwendete Systemabbilder löschen:"

#: src/ContainerDeleteModal.jsx:18
msgid "Deleting a container will erase all data in it."
msgstr "Beim Löschen des Containers werden alle Daten darin verloren gehen."

#: src/PodActions.jsx:179
msgid "Deleting this pod will remove the following containers:"
msgstr "Löschen des Pods wird die folgenden Container entfernen:"

#: src/Containers.jsx:394 src/Images.jsx:139 src/ImageRunModal.jsx:836
msgid "Details"
msgstr "Details"

#: src/Images.jsx:165
msgid "Disk space"
msgstr "Festplattenplatz"

#: src/ContainerCheckpointModal.jsx:51
msgid "Do not include root file-system changes when exporting"
msgstr "Keine root-Dateisystem-Änderungen beifügen, wenn exportieren"

#: src/ContainerCommitModal.jsx:137
msgid ""
"Docker format is useful when sharing the image with Docker or Moby Engine"
msgstr ""

#: src/ImageSearchModal.jsx:228
msgid "Download"
msgstr "Herunterladen"

#: src/Images.jsx:322
msgid "Download new image"
msgstr "Neues Abbild herunterladen"

#: src/ImageDetails.jsx:21
msgid "Entrypoint"
msgstr "Einsprungspunkt"

#: src/ImageRunModal.jsx:1030
msgid "Environment variables"
msgstr "Umgebungsvariablen"

#: src/util.js:11
msgid "Error"
msgstr "Fehler"

#: src/Notification.jsx:42 src/Images.jsx:54
msgid "Error message"
msgstr "Fehlermeldung"

#: src/ContainerTerminal.jsx:265
msgid "Error occurred while connecting console"
msgstr "Beim Verbinden der Konsole ist ein Fehler aufgetreten"

#: src/ContainerCommitModal.jsx:118
msgid "Example, Your Name <yourname@example.com>"
msgstr "Beispiel, Ihr Name <yourname@example.com>"

#: src/ImageRunModal.jsx:845
msgid "Example: $0"
msgstr "Beispiel: $0"

#: src/util.js:8 src/util.js:11 src/ContainerDetails.jsx:13
msgid "Exited"
msgstr "Beendet"

#: src/Containers.jsx:134
msgid "Failed to checkpoint container $0"
msgstr ""

#: src/ImageRunModal.jsx:448
msgid "Failed to clean up container"
msgstr "Fehler bei der Bereinigung des Containers"

#: src/ContainerCommitModal.jsx:90
msgid "Failed to commit container $0"
msgstr "Konnte Container $0 nicht committen"

#: src/ImageRunModal.jsx:509
msgid "Failed to create container $0"
msgstr "Fehler bei der Erstellung des Containers $0"

#: src/Images.jsx:52
msgid "Failed to download image $0:$1"
msgstr "Konnte Image $0 nicht herunterladen: $1"

#: src/Containers.jsx:167
msgid "Failed to force remove container $0"
msgstr "Konnte Container $0 nicht neu entfernen"

#: src/Images.jsx:372
msgid "Failed to force remove image $0"
msgstr "Fehler bei der erzwungenen Entfernung des Abbilds $0"

#: src/PodActions.jsx:89
#, fuzzy
#| msgid "Failed to restart container $0"
msgid "Failed to force restart pod $0"
msgstr "Konnte Container $0 nicht neu starten"

#: src/PodActions.jsx:67
#, fuzzy
#| msgid "Failed to stop container $0"
msgid "Failed to force stop pod $0"
msgstr "Konnte Container $0 nicht stoppen"

#: src/Containers.jsx:96
#, fuzzy
#| msgid "Failed to start container $0"
msgid "Failed to pause container $0"
msgstr "Konnte Container $0 nicht starten"

#: src/PodActions.jsx:134
#, fuzzy
#| msgid "Failed to restart container $0"
msgid "Failed to pause pod $0"
msgstr "Konnte Container $0 nicht neu starten"

#: src/PruneUnusedImagesModal.jsx:75
#, fuzzy
#| msgid "Failed to remove container $0"
msgid "Failed to prune unused images"
msgstr "Konnte Container $0 nicht neu entfernen"

#: src/ImageRunModal.jsx:515
#, fuzzy
#| msgid "Failed to remove container $0"
msgid "Failed to pull image $0"
msgstr "Konnte Container $0 nicht neu entfernen"

#: src/Containers.jsx:123
msgid "Failed to remove container $0"
msgstr "Konnte Container $0 nicht neu entfernen"

#: src/Images.jsx:362
msgid "Failed to remove image $0"
msgstr "Fehler bei der Entfernung von Abbild $0"

#: src/Containers.jsx:110
msgid "Failed to restart container $0"
msgstr "Konnte Container $0 nicht neu starten"

#: src/PodActions.jsx:78
#, fuzzy
#| msgid "Failed to restart container $0"
msgid "Failed to restart pod $0"
msgstr "Konnte Container $0 nicht neu starten"

#: src/Containers.jsx:149
#, fuzzy
#| msgid "Failed to restart container $0"
msgid "Failed to restore container $0"
msgstr "Konnte Container $0 nicht neu starten"

#: src/Containers.jsx:86
#, fuzzy
#| msgid "Failed to remove container $0"
msgid "Failed to resume container $0"
msgstr "Konnte Container $0 nicht neu entfernen"

#: src/PodActions.jsx:119
#, fuzzy
#| msgid "Failed to remove container $0"
msgid "Failed to resume pod $0"
msgstr "Konnte Container $0 nicht neu entfernen"

#: src/ImageRunModal.jsx:502
#, fuzzy
#| msgid "Failed to start container $0"
msgid "Failed to run container $0"
msgstr "Konnte Container $0 nicht starten"

#: src/ImageSearchModal.jsx:109 src/ImageRunModal.jsx:588
#, fuzzy
#| msgid "Failed to search for new images"
msgid "Failed to search for images: $0"
msgstr "Konnte nicht nach neuen Images suchen"

#: src/ImageSearchModal.jsx:108 src/ImageRunModal.jsx:586
msgid "Failed to search for new images"
msgstr "Konnte nicht nach neuen Images suchen"

#: src/Containers.jsx:76
msgid "Failed to start container $0"
msgstr "Konnte Container $0 nicht starten"

#: src/PodActions.jsx:104
#, fuzzy
#| msgid "Failed to start container $0"
msgid "Failed to start pod $0"
msgstr "Konnte Container $0 nicht starten"

#: src/Containers.jsx:66
msgid "Failed to stop container $0"
msgstr "Konnte Container $0 nicht stoppen"

#: src/PodActions.jsx:56
msgid "Failed to stop pod $0"
msgstr "Konnte Pod $0 nicht stoppen"

#: src/ContainerCommitModal.jsx:162
msgid "Force commit"
msgstr ""

#: src/ForceRemoveModal.jsx:19 src/PodActions.jsx:173
msgid "Force delete"
msgstr "Löschen erzwingen"

#: src/Containers.jsx:190 src/PodActions.jsx:93
msgid "Force restart"
msgstr "Neustart erzwingen"

#: src/Containers.jsx:182 src/PodActions.jsx:71
msgid "Force stop"
msgstr "Stoppen erzwingen"

#: src/ContainerDetails.jsx:74
msgid "Gateway"
msgstr "Gateway"

#: src/ImageRunModal.jsx:924
msgid "GiB"
msgstr "GiB"

#: src/Images.jsx:280
msgid "Hide images"
msgstr "Abbilder ausblenden"

#: src/Images.jsx:235
msgid "Hide intermediate images"
msgstr "Intermediate Images verstecken"

#: src/ImageRunModal.jsx:175
msgid "Host path"
msgstr "Host-Pfad"

#: src/ImageRunModal.jsx:71
msgid "Host port"
msgstr "Host-Port"

#: src/ImageRunModal.jsx:74
msgid "Host port help"
msgstr "Hilfe zum Host-Port"

#: src/ContainerDetails.jsx:52 src/Images.jsx:164
msgid "ID"
msgstr "ID"

#: src/ContainerDetails.jsx:70 src/ImageRunModal.jsx:55
msgid "IP address"
msgstr "IP-Adresse"

#: src/ImageRunModal.jsx:58
msgid "IP address help"
msgstr "Hilfe zur IP-Adresse"

#: src/ImageRunModal.jsx:60
msgid ""
"If host IP is set to 0.0.0.0 or not set at all, the port will be bound on "
"all IPs on the host."
msgstr ""

#: src/ImageRunModal.jsx:76
msgid ""
"If the host port is not set the container port will be randomly assigned a "
"port on the host."
msgstr ""

#: src/ContainerRestoreModal.jsx:49
msgid "Ignore IP address if set statically"
msgstr "IP-Adresse ignorieren, wenn statisch festgelegt"

#: src/ContainerRestoreModal.jsx:52
msgid "Ignore MAC address if set statically"
msgstr "MAC-Adresse ignorieren, wenn statisch festgelegt"

#: src/ContainerDetails.jsx:56 src/Images.jsx:161 src/ImageRunModal.jsx:838
msgid "Image"
msgstr "Image"

#: src/ImageRunModal.jsx:840
msgid "Image selection help"
msgstr "Abbildauswahl-Hilfe"

#: src/Images.jsx:240 src/Images.jsx:268
msgid "Images"
msgstr "Images"

#: src/ImageRunModal.jsx:1006
msgid "Integration"
msgstr "Integration"

#: src/ContainerCheckpointModal.jsx:43 src/ContainerRestoreModal.jsx:44
msgid "Keep all temporary checkpoint files"
msgstr ""

#: src/ImageRunModal.jsx:150
msgid "Key"
msgstr "Schlüssel"

#: src/ImageRunModal.jsx:922
msgid "KiB"
msgstr "KiB"

#: src/ContainerCheckpointModal.jsx:45
msgid "Leave running after writing checkpoint to disk"
msgstr ""

#: src/ContainerLogs.jsx:53
msgid "Loading logs..."
msgstr "Protokolle werden geladen ..."

#: src/Containers.jsx:438 src/ImageUsedBy.jsx:9
msgid "Loading..."
msgstr "Wird geladen ..."

#: src/ImageRunModal.jsx:789
msgid "Local"
msgstr "Lokal"

#: src/ImageRunModal.jsx:676
msgid "Local images"
msgstr "Lokale Abbilder"

#: src/Containers.jsx:398
msgid "Logs"
msgstr "Protokolle"

#: src/ContainerDetails.jsx:78
msgid "MAC address"
msgstr "MAC-Adresse"

#: src/ImageRunModal.jsx:984
msgid "Maximum retries"
msgstr ""

#: src/Containers.jsx:428
msgid "Memory"
msgstr "Speicher"

#: src/ImageRunModal.jsx:902
msgid "Memory limit"
msgstr "Speicher-Limit"

#: src/ImageRunModal.jsx:917
msgid "Memory unit"
msgstr "Speichereinheit"

#: src/ImageRunModal.jsx:923
msgid "MiB"
msgstr "MiB"

#: src/ImageRunModal.jsx:185
msgid "Mode"
msgstr "Modus"

#: src/ImageDeleteModal.jsx:66
msgid "Multiple tags exist for this image. Select the tagged images to delete."
msgstr ""

#: src/ImageRunModal.jsx:815
msgid "Name"
msgstr "Name"

#: src/ContainerCommitModal.jsx:100
msgid "New image name"
msgstr "Neuer Abbildname"

#: src/ImageRunModal.jsx:976
msgid "No"
msgstr "Nein"

#: src/Containers.jsx:435
msgid "No containers"
msgstr "Keine Container"

#: src/ImageUsedBy.jsx:11
msgid "No containers are using this image"
msgstr "Keine Container benutzen dieses Abbild"

#: src/Containers.jsx:436
msgid "No containers in this pod"
msgstr ""

#: src/Containers.jsx:440
msgid "No containers that match the current filter"
msgstr "Aktueller Filter passt auf keinen Container"

#: src/ImageRunModal.jsx:1028
msgid "No environment variables specified"
msgstr "Keine Umgebungsvariablen angegeben"

#: src/Images.jsx:169
msgid "No images"
msgstr "Keine Images"

#: src/ImageSearchModal.jsx:176 src/ImageRunModal.jsx:863
msgid "No images found"
msgstr "Keine Abbilder gefunden"

#: src/Images.jsx:173
msgid "No images that match the current filter"
msgstr "Aktueller Filter passt auf kein Image"

#: src/ImageRunModal.jsx:196
msgid "No label"
msgstr "Keine Bezeichnung"

#: src/ImageRunModal.jsx:1009
msgid "No ports exposed"
msgstr "Keine offenen Ports"

#: src/ImageSearchModal.jsx:180
msgid "No results for $0"
msgstr "Keine Ergebnisse für $0"

#: src/Containers.jsx:442
msgid "No running containers"
msgstr "Keine laufenden Container"

#: src/ImageRunModal.jsx:1018
msgid "No volumes specified"
msgstr "Keine Volumen angegeben"

#: src/ImageRunModal.jsx:977
msgid "On failure"
msgstr "Bei einem Ausfall"

#: src/Containers.jsx:534
msgid "Only running"
msgstr "Nur laufenlassen"

#: src/ContainerCommitModal.jsx:129 src/ImageRunModal.jsx:999
msgid "Options"
msgstr "Optionen"

#: src/ContainerHeader.jsx:43 src/Containers.jsx:426 src/Images.jsx:162
#: src/ImageSearchModal.jsx:148 src/ImageRunModal.jsx:823
msgid "Owner"
msgstr "Eigentümer"

#: src/ImageRunModal.jsx:1034
msgid ""
"Paste one or more lines of key=value pairs into any field for bulk import"
msgstr ""

#: src/Containers.jsx:198 src/PodActions.jsx:138
msgid "Pause"
msgstr "Anhalten"

#: src/ContainerCommitModal.jsx:133
msgid "Pause container when creating image"
msgstr "Container bei der Abbilderstellung pausieren"

#: src/util.js:8 src/util.js:11
msgid "Paused"
msgstr "Pausiert"

#: src/ContainerDeleteModal.jsx:12
msgid "Please confirm deletion of $0"
msgstr "Bitte bestätigen Sie das Löschen von $0"

#: src/PodActions.jsx:170
msgid "Please confirm deletion of pod $0"
msgstr "Bitte bestätigen Sie das Löschen von Pod $0"

#: src/PodActions.jsx:170
msgid "Please confirm force deletion of pod $0"
msgstr "Bitte bestätigen Sie das erzwungene Löschen von pod $0"

#: src/ForceRemoveModal.jsx:14
msgid "Please confirm forced deletion of $0"
msgstr "Bitte bestätigen Sie das erzwungene Löschen von $0"

#: src/ImageSearchModal.jsx:181
msgid "Please retry another term."
msgstr ""

#: src/ImageSearchModal.jsx:176
msgid "Please start typing to look for images."
msgstr ""

#: src/index.html:20 src/manifest.json:0
msgid "Podman containers"
msgstr "Podman-Container"

#: src/app.jsx:583
msgid "Podman service is not active"
msgstr "Podman-Service ist nicht aktiv"

#: src/ImageRunModal.jsx:1011
msgid "Port mapping"
msgstr "Portzuordnung"

#: src/ContainerDetails.jsx:66 src/ImageDetails.jsx:39
msgid "Ports"
msgstr "Ports"

#: src/ImageRunModal.jsx:198
msgid "Private"
msgstr "Privat"

#: src/ImageRunModal.jsx:102
msgid "Protocol"
msgstr "Protokoll"

#: src/PruneUnusedImagesModal.jsx:101
msgid "Prune"
msgstr ""

#: src/Images.jsx:331 src/PruneUnusedImagesModal.jsx:94
msgid "Prune unused images"
msgstr ""

#: src/PruneUnusedImagesModal.jsx:97 src/PruneUnusedImagesModal.jsx:101
msgid "Pruning images"
msgstr ""

#: src/ImageRunModal.jsx:884
#, fuzzy
#| msgid "Delete image"
msgid "Pull latest image"
msgstr "Image löschen"

#: src/Images.jsx:303
msgid "Pulling"
msgstr "Herunterladen"

#: src/ImageRunModal.jsx:117 src/ImageRunModal.jsx:165
#: src/ImageRunModal.jsx:205
msgid "Remove item"
msgstr "Element entfernen"

#: src/util.js:8
#, fuzzy
#| msgid "removing"
msgid "Removing"
msgstr "Entfernen"

#: src/Containers.jsx:186 src/PodActions.jsx:82
msgid "Restart"
msgstr "Neustart"

#: src/ImageRunModal.jsx:961
msgid "Restart policy"
msgstr "Neustartrichtlinie"

#: src/ImageRunModal.jsx:963 src/ImageRunModal.jsx:973
msgid "Restart policy help"
msgstr "Neustartrichtlinien-Hilfe"

#: src/ImageRunModal.jsx:965
msgid "Restart policy to follow when containers exit."
msgstr ""

#: src/Containers.jsx:233 src/ContainerRestoreModal.jsx:35
msgid "Restore"
msgstr "Wiederherstellen"

#: src/ContainerRestoreModal.jsx:30
msgid "Restore container $0"
msgstr "Container $0 wiederherstellen"

#: src/ContainerRestoreModal.jsx:46
msgid "Restore with established TCP connections"
msgstr "Wiederherstellung mit bestehenden TCP-Verbindungen"

#: src/Containers.jsx:205 src/PodActions.jsx:123
msgid "Resume"
msgstr "Fotfahren"

#: src/util.js:8 src/util.js:11 src/ImageUsedBy.jsx:32
msgid "Running"
msgstr "Läuft"

#: src/ImageRunModal.jsx:192
msgid "SELinux"
msgstr "SELinux"

#: src/ImageSearchModal.jsx:156
msgid "Search by name or description"
msgstr "Nach Name oder Beschreibung suchen"

#: src/ImageRunModal.jsx:779
msgid "Search by registry"
msgstr ""

#: src/ImageSearchModal.jsx:153
msgid "Search for"
msgstr "Suchen nach"

#: src/ImageSearchModal.jsx:214
msgid "Search for an image"
msgstr "Nach einem Abbild suchen"

#: src/ImageRunModal.jsx:868
msgid "Search string or container location"
msgstr ""

#: src/ImageSearchModal.jsx:174
msgid "Searching..."
msgstr "Wird gesucht ..."

#: src/ImageRunModal.jsx:846
msgid "Searching: $0"
msgstr "Wird gesucht: $0"

#: src/ImageRunModal.jsx:197
msgid "Shared"
msgstr "Freigegeben"

#: src/Containers.jsx:530
msgid "Show"
msgstr "Anzeigen"

#: src/Images.jsx:280
msgid "Show images"
msgstr "Abbilder anzeigen"

#: src/Images.jsx:235
msgid "Show intermediate images"
msgstr "Zwischenabbilder anzeigen"

#: src/PruneUnusedImagesModal.jsx:44
msgid "Show more"
msgstr "Mehr anzeigen"

#: src/Containers.jsx:225 src/app.jsx:624 src/PodActions.jsx:108
msgid "Start"
msgstr "Starten"

#: src/ImageRunModal.jsx:1002
msgid "Start after creation"
msgstr "Nach der Erstellung starten"

#: src/app.jsx:590
msgid "Start podman"
msgstr "Podman starten"

#: src/Containers.jsx:429 src/ContainerDetails.jsx:88
msgid "State"
msgstr "Zustand"

#: src/Containers.jsx:178 src/PodActions.jsx:60
msgid "Stop"
msgstr "Stoppen"

#: src/util.js:8 src/util.js:11
msgid "Stopped"
msgstr "Angehalten"

#: src/ContainerCheckpointModal.jsx:48
msgid "Support preserving established TCP connections"
msgstr "Unterstützung der Aufrechterhaltung bestehender TCP-Verbindungen"

#: src/ContainerHeader.jsx:48 src/ImageRunModal.jsx:825
msgid "System"
msgstr "System"

#: src/app.jsx:629
msgid "System Podman service is also available"
msgstr "System-Podman ist auch verfügbar"

#: src/ImageRunModal.jsx:108
msgid "TCP"
msgstr "TCP"

#: src/ContainerCommitModal.jsx:109 src/ImageSearchModal.jsx:218
msgid "Tag"
msgstr "Etikett"

#: src/ImageDetails.jsx:27
msgid "Tags"
msgstr "Etiketten"

#: src/app.jsx:595
msgid "Troubleshoot"
msgstr "Fehler suchen"

#: src/ContainerHeader.jsx:56
msgid "Type to filter…"
msgstr "Tippen zum filtern…"

#: src/ImageRunModal.jsx:109
msgid "UDP"
msgstr "UDP"

#: src/ContainerDetails.jsx:11
msgid "Up since $0"
msgstr "Läuft seit $0"

#: src/ContainerCommitModal.jsx:138
msgid "Use legacy Docker format"
msgstr "Veraltetes Docker-Format verwenden"

#: src/Images.jsx:166 src/ImageDetails.jsx:33
msgid "Used by"
msgstr "Benutzt von"

#: src/app.jsx:66 src/app.jsx:512
msgid "User"
msgstr "Benutzer"

#: src/app.jsx:634
msgid "User Podman service is also available"
msgstr "Benutzer Podman ist auch verfügbar"

#: src/ImageRunModal.jsx:827
msgid "User:"
msgstr "Benutzer:"

#: src/ImageRunModal.jsx:155
msgid "Value"
msgstr "Wert"

#: src/ImageRunModal.jsx:1020
msgid "Volumes"
msgstr "Volumen"

#: src/ImageRunModal.jsx:898
msgid "With terminal"
msgstr "Mit Terminal"

#: src/ImageRunModal.jsx:187
msgid "Writable"
msgstr "Beschreibbar"

#: src/manifest.json
msgid "container"
msgstr "Container"

#: src/ImageRunModal.jsx:486
msgid "downloading"
msgstr "wird heruntergeladen"

#: src/ImageRunModal.jsx:844
msgid "host[:port]/[user]/container[:tag]"
msgstr "Host[:Port]/[Benutzer]/Container[:Tag]"

#: src/manifest.json
msgid "image"
msgstr "Image"

#: src/ImageSearchModal.jsx:161
msgid "in"
msgstr "in"

#: src/Containers.jsx:358 src/Containers.jsx:359
msgid "n/a"
msgstr "n.v."

#: src/Containers.jsx:358 src/Containers.jsx:359
msgid "not available"
msgstr "nicht verfügbar"

#: src/Containers.jsx:612
msgid "pod group"
msgstr "Pod-Gruppe"

#: src/manifest.json
msgid "podman"
msgstr "podman"

#: src/ImageDeleteModal.jsx:80
msgid "select all"
msgstr "Alles auswählen"

#: src/Containers.jsx:381 src/Images.jsx:123 src/ImageSearchModal.jsx:149
msgid "system"
msgstr "System"

#: src/Images.jsx:74 src/Images.jsx:81
msgid "unused"
msgstr "ungenutzt"

#: src/Containers.jsx:381 src/Images.jsx:123
msgid "user:"
msgstr "Benutzer:"

#~ msgid "Delete unused $0 images:"
#~ msgstr "Nicht verwendete $0 Abbilder löschen:"

#~ msgid "created"
#~ msgstr "erstellt"

#~ msgid "exited"
#~ msgstr "beendet"

#~ msgid "paused"
#~ msgstr "pausiert"

#~ msgid "running"
#~ msgstr "Läuft"

#~ msgid "stopped"
#~ msgstr "Angehalten"

#~ msgid "user"
#~ msgstr "Benutzer"

#~ msgid "Add on build variable"
#~ msgstr "Build-Variable hinzufügen"

#~ msgid "Commit image"
#~ msgstr "Image committen"

#~ msgid "Format"
#~ msgstr "Format"

#~ msgid "Message"
#~ msgstr "Nachricht"

#~ msgid "Pause the container"
#~ msgstr "Den Container anhalten"

#~ msgid "Remove on build variable"
#~ msgstr "Bei Bau-Variable entfernen"

#~ msgid "Add item"
#~ msgstr "Element hinzufügen"

#, fuzzy
#~| msgid "Host port"
#~ msgid "Host port (optional)"
#~ msgstr "Host-Port"

#~ msgid "ReadOnly"
#~ msgstr "Nur lesen"

#~ msgid "ReadWrite"
#~ msgstr "Lesen und Schreiben"

#~ msgid "IP prefix length"
#~ msgstr "IP-Präfixlänge"

#~ msgid "Get new image"
#~ msgstr "Neues Image herunterladen"

#~ msgid "Run"
#~ msgstr "Starten"

#~ msgid "Size"
#~ msgstr "Größe"

#~ msgid "On build"
#~ msgstr "Beim Bauen"

#~ msgid "Are you sure you want to delete this image?"
#~ msgstr "Sind Sie sicher, dass Sie dieses Image löschen wollen?"

#~ msgid "Could not attach to this container: $0"
#~ msgstr "Konnte nicht mit diesem Container verbinden: $0"

#~ msgid "Could not open channel: $0"
#~ msgstr "Konnte keinen Kanal öffnen: $0"

#~ msgid "Everything"
#~ msgstr "Alles"

#~ msgid "Images and running containers"
#~ msgstr "Images und laufende Container"

#~ msgid "Security"
#~ msgstr "Sicherheit"

#~ msgid "This version of the Web Console does not support a terminal."
#~ msgstr "Diese Version der Web-Konsole unterstützt kein Terminal."
