# #-#-#-#-#  podman.js.pot (PACKAGE VERSION)  #-#-#-#-#
# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Oğuz Ersen <oguzersen@protonmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE_VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-31 02:51+0000\n"
"PO-Revision-Date: 2022-01-23 11:07+0000\n"
"Last-Translator: Burak Yavuz <hitowerdigit@hotmail.com>\n"
"Language-Team: Turkish <https://translate.fedoraproject.org/projects/cockpit-"
"podman/main/tr/>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1\n"
"X-Generator: Weblate 4.10.1\n"

#: src/Images.jsx:78
msgid "$0 container"
msgid_plural "$0 containers"
msgstr[0] "$0 kapsayıcı"
msgstr[1] "$0 kapsayıcı"

#: src/Images.jsx:253
msgid "$0 image total, $1"
msgid_plural "$0 images total, $1"
msgstr[0] "$0 kalıp toplam, $1"
msgstr[1] "$0 kalıp toplam, $1"

#: src/Images.jsx:257
msgid "$0 unused image, $1"
msgid_plural "$0 unused images, $1"
msgstr[0] "$0 kullanılmayan kalıp, $1"
msgstr[1] "$0 kullanılmayan kalıp, $1"

#: src/ImageRunModal.jsx:1012
msgid "Add port mapping"
msgstr "Bağlantı noktası eşlemesi ekle"

#: src/ImageRunModal.jsx:1031
msgid "Add variable"
msgstr "Değişken ekle"

#: src/ImageRunModal.jsx:1021
msgid "Add volume"
msgstr "Birim ekle"

#: src/ContainerHeader.jsx:49 src/Containers.jsx:535 src/ImageRunModal.jsx:780
msgid "All"
msgstr "Tümü"

#: src/ImageSearchModal.jsx:167
msgid "All registries"
msgstr "Tüm kayıtlar"

#: src/ImageRunModal.jsx:978
msgid "Always"
msgstr "Her zaman"

#: src/PodActions.jsx:178
msgid "An error occurred"
msgstr "Bir hata meydana geldi"

#: src/ContainerCommitModal.jsx:116
msgid "Author"
msgstr "Hazırlayan"

#: src/app.jsx:587
msgid "Automatically start podman on boot"
msgstr "Podman'ı önyüklemede otomatik olarak başlat"

#: src/Containers.jsx:427
msgid "CPU"
msgstr "CPU"

#: src/ImageRunModal.jsx:934
msgid "CPU Shares help"
msgstr "CPU Paylaşımları yardımı"

#: src/ImageRunModal.jsx:932
msgid "CPU shares"
msgstr "CPU paylaşımları"

#: src/ImageRunModal.jsx:936
msgid ""
"CPU shares determine the priority of running containers. Default priority is "
"1024. A higher number prioritizes this container. A lower number decreases "
"priority."
msgstr ""
"CPU paylaşımları, kapsayıcıları çalıştırmanın önceliğini belirler. "
"Varsayılan öncelik 1024'dür. Daha yüksek bir sayı bu kapsayıcıya öncelik "
"verir. Daha düşük bir sayı önceliği azaltır."

#: src/ContainerCheckpointModal.jsx:38 src/ForceRemoveModal.jsx:21
#: src/ContainerRestoreModal.jsx:39 src/ContainerCommitModal.jsx:168
#: src/ContainerDeleteModal.jsx:15 src/ImageSearchModal.jsx:231
#: src/PodActions.jsx:174 src/PruneUnusedImagesModal.jsx:103
#: src/ImageRunModal.jsx:1059 src/ImageDeleteModal.jsx:62
msgid "Cancel"
msgstr "İptal"

#: src/ContainerCheckpointModal.jsx:34 src/Containers.jsx:215
msgid "Checkpoint"
msgstr "Denetim noktası"

#: src/ContainerCheckpointModal.jsx:29
msgid "Checkpoint container $0"
msgstr "$0 kapsayıcısı denetim noktası"

#: src/ContainerCommitModal.jsx:123 src/ContainerDetails.jsx:60
#: src/ImageDetails.jsx:15 src/ImageRunModal.jsx:889
msgid "Command"
msgstr "Komut"

#: src/Containers.jsx:243 src/ContainerCommitModal.jsx:155
msgid "Commit"
msgstr "İşle"

#: src/ContainerCommitModal.jsx:146
msgid "Commit container"
msgstr "Kapsayıcı işle"

#: src/util.js:8
msgid "Configured"
msgstr "Yapılandırıldı"

#: src/Containers.jsx:402
msgid "Console"
msgstr "Konsol"

#: src/Containers.jsx:425
msgid "Container"
msgstr "Kapsayıcı"

#: src/ImageRunModal.jsx:459
msgid "Container failed to be created"
msgstr "Kapsayıcının oluşturulması başarısız oldu"

#: src/ImageRunModal.jsx:442
msgid "Container failed to be started"
msgstr "Kapsayıcının başlatılması başarısız oldu"

#: src/Containers.jsx:288
msgid "Container is currently running."
msgstr "Kapsayıcı şu anda çalışıyor."

#: src/ContainerTerminal.jsx:262
msgid "Container is not running"
msgstr "Kapsayıcı çalışmıyor"

#: src/ImageRunModal.jsx:818
msgid "Container name"
msgstr "Kapsayıcı adı"

#: src/ImageRunModal.jsx:180
msgid "Container path"
msgstr "Kapsayıcı yolu"

#: src/ImageRunModal.jsx:91
msgid "Container port"
msgstr "Kapsayıcı bağlantı noktası"

#: src/Containers.jsx:566 src/Containers.jsx:573 src/Containers.jsx:601
msgid "Containers"
msgstr "Kapsayıcılar"

#: src/ImageRunModal.jsx:1056
msgid "Create"
msgstr "Oluştur"

#: src/ContainerCommitModal.jsx:147
msgid "Create a new image based on the current state of the $0 container."
msgstr "$0 kapsayıcısının şu anki durumuna göre yeni bir kalıp oluşturun."

#: src/Containers.jsx:544 src/Images.jsx:388 src/ImageRunModal.jsx:1052
msgid "Create container"
msgstr "Kapsayıcı oluştur"

#: src/ImageRunModal.jsx:1052
msgid "Create container in $0"
msgstr "$0 içinde kapsayıcı oluştur"

#: src/Containers.jsx:620
msgid "Create container in pod"
msgstr "Bölme içinde kapsayıcı oluştur"

#: src/util.js:8 src/util.js:11 src/ContainerDetails.jsx:84 src/Images.jsx:163
msgid "Created"
msgstr "Oluşturuldu"

#: src/ImageRunModal.jsx:824
msgid "Default with single selectable"
msgstr "Tek seçilebilir ile varsayılan"

#: src/Containers.jsx:251 src/ContainerDeleteModal.jsx:14 src/Images.jsx:401
#: src/PodActions.jsx:151 src/PodActions.jsx:173
msgid "Delete"
msgstr "Sil"

#: src/ImageDeleteModal.jsx:56
msgid "Delete $0"
msgstr "$0 sil"

#: src/ImageDeleteModal.jsx:60
msgid "Delete tagged images"
msgstr "Etiketli kalıpları sil"

#: src/PruneUnusedImagesModal.jsx:28
msgid "Delete unused system images:"
msgstr "Kullanılmayan sistem kalıplarını sil:"

#: src/PruneUnusedImagesModal.jsx:28
msgid "Delete unused user images:"
msgstr "Kullanılmayan kullanıcı kalıplarını sil:"

#: src/ContainerDeleteModal.jsx:18
msgid "Deleting a container will erase all data in it."
msgstr "Bir kapsayıcıyı silmek içindeki tüm verileri silecektir."

#: src/PodActions.jsx:179
msgid "Deleting this pod will remove the following containers:"
msgstr "Bu bölmeyi silmek aşağıdaki kapsayıcıları kaldıracaktır:"

#: src/Containers.jsx:394 src/Images.jsx:139 src/ImageRunModal.jsx:836
msgid "Details"
msgstr "Ayrıntılar"

#: src/Images.jsx:165
msgid "Disk space"
msgstr "Disk alanı"

#: src/ContainerCheckpointModal.jsx:51
msgid "Do not include root file-system changes when exporting"
msgstr "Dışa aktarırken kök dosya sistemi değişikliklerini dahil etme"

#: src/ContainerCommitModal.jsx:137
msgid ""
"Docker format is useful when sharing the image with Docker or Moby Engine"
msgstr ""
"Docker biçimi, kalıbı Docker veya Moby Engine ile paylaşırken kullanışlıdır"

#: src/ImageSearchModal.jsx:228
msgid "Download"
msgstr "İndir"

#: src/Images.jsx:322
msgid "Download new image"
msgstr "Yeni kalıbı indir"

#: src/ImageDetails.jsx:21
msgid "Entrypoint"
msgstr "Giriş noktası"

#: src/ImageRunModal.jsx:1030
msgid "Environment variables"
msgstr "Ortam değişkenleri"

#: src/util.js:11
msgid "Error"
msgstr "Hata"

#: src/Notification.jsx:42 src/Images.jsx:54
msgid "Error message"
msgstr "Hata iletisi"

#: src/ContainerTerminal.jsx:265
msgid "Error occurred while connecting console"
msgstr "Konsola bağlanırken hata meydana geldi"

#: src/ContainerCommitModal.jsx:118
msgid "Example, Your Name <yourname@example.com>"
msgstr "Örnek, Adınız <adiniz@ornek.com>"

#: src/ImageRunModal.jsx:845
msgid "Example: $0"
msgstr "Örnek: $0"

#: src/util.js:8 src/util.js:11 src/ContainerDetails.jsx:13
msgid "Exited"
msgstr "Çıkıldı"

#: src/Containers.jsx:134
msgid "Failed to checkpoint container $0"
msgstr "$0 kapsayıcısını denetleme noktası başarısız oldu"

#: src/ImageRunModal.jsx:448
msgid "Failed to clean up container"
msgstr "Kapsayıcıyı temizleme başarısız oldu"

#: src/ContainerCommitModal.jsx:90
msgid "Failed to commit container $0"
msgstr "$0 kapsayıcısını işleme başarısız oldu"

#: src/ImageRunModal.jsx:509
msgid "Failed to create container $0"
msgstr "$0 kapsayıcısını geri yükleme başarısız oldu"

#: src/Images.jsx:52
msgid "Failed to download image $0:$1"
msgstr "$0:$1 kalıbını indirme başarısız oldu"

#: src/Containers.jsx:167
msgid "Failed to force remove container $0"
msgstr "$0 kapsayıcısını zorla kaldırma başarısız oldu"

#: src/Images.jsx:372
msgid "Failed to force remove image $0"
msgstr "$0 kalıbını zorla kaldırma başarısız oldu"

#: src/PodActions.jsx:89
msgid "Failed to force restart pod $0"
msgstr "$0 bölmesini yeniden başlatmaya zorlama başarısız oldu"

#: src/PodActions.jsx:67
msgid "Failed to force stop pod $0"
msgstr "$0 bölmesini zorla durdurma başarısız oldu"

#: src/Containers.jsx:96
msgid "Failed to pause container $0"
msgstr "$0 kapsayıcısını duraklatma başarısız oldu"

#: src/PodActions.jsx:134
msgid "Failed to pause pod $0"
msgstr "$0 bölmesini duraklatma başarısız oldu"

#: src/PruneUnusedImagesModal.jsx:75
msgid "Failed to prune unused images"
msgstr "Kullanılmayan kalıpları ayıklama başarısız oldu"

#: src/ImageRunModal.jsx:515
msgid "Failed to pull image $0"
msgstr "$0 kalıbını çekme başarısız oldu"

#: src/Containers.jsx:123
msgid "Failed to remove container $0"
msgstr "$0 kapsayıcısını kaldırma başarısız oldu"

#: src/Images.jsx:362
msgid "Failed to remove image $0"
msgstr "$0 kalıbını kaldırma başarısız oldu"

#: src/Containers.jsx:110
msgid "Failed to restart container $0"
msgstr "$0 kapsayıcısını yeniden başlatma başarısız oldu"

#: src/PodActions.jsx:78
msgid "Failed to restart pod $0"
msgstr "$0 bölmesini yeniden başlatma başarısız oldu"

#: src/Containers.jsx:149
msgid "Failed to restore container $0"
msgstr "$0 kapsayıcısını geri yükleme başarısız oldu"

#: src/Containers.jsx:86
msgid "Failed to resume container $0"
msgstr "$0 kapsayıcısını sürdürme başarısız oldu"

#: src/PodActions.jsx:119
msgid "Failed to resume pod $0"
msgstr "$0 bölmesini sürdürme başarısız oldu"

#: src/ImageRunModal.jsx:502
msgid "Failed to run container $0"
msgstr "$0 kapsayıcısını çalıştırma başarısız oldu"

#: src/ImageSearchModal.jsx:109 src/ImageRunModal.jsx:588
msgid "Failed to search for images: $0"
msgstr "Kalıpları arama başarısız oldu: $0"

#: src/ImageSearchModal.jsx:108 src/ImageRunModal.jsx:586
msgid "Failed to search for new images"
msgstr "Yeni kalıpları arama başarısız oldu"

#: src/Containers.jsx:76
msgid "Failed to start container $0"
msgstr "$0 kapsayıcısını başlatma başarısız oldu"

#: src/PodActions.jsx:104
msgid "Failed to start pod $0"
msgstr "$0 bölmesini başlatma başarısız oldu"

#: src/Containers.jsx:66
msgid "Failed to stop container $0"
msgstr "$0 kapsayıcısını durdurma başarısız oldu"

#: src/PodActions.jsx:56
msgid "Failed to stop pod $0"
msgstr "$0 bölmesini durdurma başarısız oldu"

#: src/ContainerCommitModal.jsx:162
msgid "Force commit"
msgstr "İşlemeye zorla"

#: src/ForceRemoveModal.jsx:19 src/PodActions.jsx:173
msgid "Force delete"
msgstr "Silmeye zorla"

#: src/Containers.jsx:190 src/PodActions.jsx:93
msgid "Force restart"
msgstr "Yeniden başlatmaya zorla"

#: src/Containers.jsx:182 src/PodActions.jsx:71
msgid "Force stop"
msgstr "Durdurmaya zorla"

#: src/ContainerDetails.jsx:74
msgid "Gateway"
msgstr "Ağ geçidi"

#: src/ImageRunModal.jsx:924
msgid "GiB"
msgstr "GiB"

#: src/Images.jsx:280
msgid "Hide images"
msgstr "Kalıpları gizle"

#: src/Images.jsx:235
msgid "Hide intermediate images"
msgstr "Ara kalıpları gizle"

#: src/ImageRunModal.jsx:175
msgid "Host path"
msgstr "Anamakine yolu"

#: src/ImageRunModal.jsx:71
msgid "Host port"
msgstr "Anamakine bağlantı noktası"

#: src/ImageRunModal.jsx:74
msgid "Host port help"
msgstr "Anamakine b.noktası yardım"

#: src/ContainerDetails.jsx:52 src/Images.jsx:164
msgid "ID"
msgstr "Kimlik"

#: src/ContainerDetails.jsx:70 src/ImageRunModal.jsx:55
msgid "IP address"
msgstr "IP adresi"

#: src/ImageRunModal.jsx:58
msgid "IP address help"
msgstr "IP adresi yardım"

#: src/ImageRunModal.jsx:60
msgid ""
"If host IP is set to 0.0.0.0 or not set at all, the port will be bound on "
"all IPs on the host."
msgstr ""
"Eğer anamakine IP'si 0.0.0.0 olarak ayarlanırsa veya hiç ayarlanmazsa, "
"bağlantı noktası anamakinedeki tüm IP'lere bağlanır."

#: src/ImageRunModal.jsx:76
msgid ""
"If the host port is not set the container port will be randomly assigned a "
"port on the host."
msgstr ""
"Eğer anamakine bağlantı noktası ayarlanmazsa, kapsayıcı bağlantı noktası "
"anamakinede rastgele bir bağlantı noktasına atanacaktır."

#: src/ContainerRestoreModal.jsx:49
msgid "Ignore IP address if set statically"
msgstr "Sabit olarak ayarlanmışsa IP adresini yoksay"

#: src/ContainerRestoreModal.jsx:52
msgid "Ignore MAC address if set statically"
msgstr "Sabit olarak ayarlanmışsa MAC adresini yoksay"

#: src/ContainerDetails.jsx:56 src/Images.jsx:161 src/ImageRunModal.jsx:838
msgid "Image"
msgstr "Kalıp"

#: src/ImageRunModal.jsx:840
msgid "Image selection help"
msgstr "Kalıp seçim yardımı"

#: src/Images.jsx:240 src/Images.jsx:268
msgid "Images"
msgstr "Kalıplar"

#: src/ImageRunModal.jsx:1006
msgid "Integration"
msgstr "Bütünleştirme"

#: src/ContainerCheckpointModal.jsx:43 src/ContainerRestoreModal.jsx:44
msgid "Keep all temporary checkpoint files"
msgstr "Tüm geçici denetim noktası dosyalarını sakla"

#: src/ImageRunModal.jsx:150
msgid "Key"
msgstr "Anahtar"

#: src/ImageRunModal.jsx:922
msgid "KiB"
msgstr "KiB"

#: src/ContainerCheckpointModal.jsx:45
msgid "Leave running after writing checkpoint to disk"
msgstr "Denetim noktasını diske yazdıktan sonra çalışır durumda bırak"

#: src/ContainerLogs.jsx:53
msgid "Loading logs..."
msgstr "Günlükler yükleniyor..."

#: src/Containers.jsx:438 src/ImageUsedBy.jsx:9
msgid "Loading..."
msgstr "Yükleniyor..."

#: src/ImageRunModal.jsx:789
msgid "Local"
msgstr "Yerel"

#: src/ImageRunModal.jsx:676
msgid "Local images"
msgstr "Yerel kalıplar"

#: src/Containers.jsx:398
msgid "Logs"
msgstr "Günlükler"

#: src/ContainerDetails.jsx:78
msgid "MAC address"
msgstr "MAC adresi"

#: src/ImageRunModal.jsx:984
msgid "Maximum retries"
msgstr "En fazla yeniden deneme"

#: src/Containers.jsx:428
msgid "Memory"
msgstr "Bellek"

#: src/ImageRunModal.jsx:902
msgid "Memory limit"
msgstr "Bellek sınırı"

#: src/ImageRunModal.jsx:917
msgid "Memory unit"
msgstr "Bellek birimi"

#: src/ImageRunModal.jsx:923
msgid "MiB"
msgstr "MiB"

#: src/ImageRunModal.jsx:185
msgid "Mode"
msgstr "Mod"

#: src/ImageDeleteModal.jsx:66
msgid "Multiple tags exist for this image. Select the tagged images to delete."
msgstr ""
"Bu kalıp için birden fazla etiket var. Silinecek etiketlenmiş kalıpları "
"seçin."

#: src/ImageRunModal.jsx:815
msgid "Name"
msgstr "Ad"

#: src/ContainerCommitModal.jsx:100
msgid "New image name"
msgstr "Yeni kalıp adı"

#: src/ImageRunModal.jsx:976
msgid "No"
msgstr "Hayır"

#: src/Containers.jsx:435
msgid "No containers"
msgstr "Kapsayıcılar yok"

#: src/ImageUsedBy.jsx:11
msgid "No containers are using this image"
msgstr "Bu kalıbı hiçbir kapsayıcı kullanmıyor"

#: src/Containers.jsx:436
msgid "No containers in this pod"
msgstr "Bu bölmede kapsayıcılar yok"

#: src/Containers.jsx:440
msgid "No containers that match the current filter"
msgstr "Şu anki süzgeçle eşleşen kapsayıcılar yok"

#: src/ImageRunModal.jsx:1028
msgid "No environment variables specified"
msgstr "Belirtilen ortam değişkenleri yok"

#: src/Images.jsx:169
msgid "No images"
msgstr "Kalıplar yok"

#: src/ImageSearchModal.jsx:176 src/ImageRunModal.jsx:863
msgid "No images found"
msgstr "Bulunan kalıplar yok"

#: src/Images.jsx:173
msgid "No images that match the current filter"
msgstr "Şu anki süzgeçle eşleşen kalıplar yok"

#: src/ImageRunModal.jsx:196
msgid "No label"
msgstr "Etiket yok"

#: src/ImageRunModal.jsx:1009
msgid "No ports exposed"
msgstr "Açığa çıkan bağlantı noktaları yok"

#: src/ImageSearchModal.jsx:180
msgid "No results for $0"
msgstr "$0 için sonuçlar yok"

#: src/Containers.jsx:442
msgid "No running containers"
msgstr "Çalışan kapsayıcılar yok"

#: src/ImageRunModal.jsx:1018
msgid "No volumes specified"
msgstr "Belirtilen birimler yok"

#: src/ImageRunModal.jsx:977
msgid "On failure"
msgstr "Başarısızlıkla"

#: src/Containers.jsx:534
msgid "Only running"
msgstr "Sadece çalışanlar"

#: src/ContainerCommitModal.jsx:129 src/ImageRunModal.jsx:999
msgid "Options"
msgstr "Seçenekler"

#: src/ContainerHeader.jsx:43 src/Containers.jsx:426 src/Images.jsx:162
#: src/ImageSearchModal.jsx:148 src/ImageRunModal.jsx:823
msgid "Owner"
msgstr "Sahibi"

#: src/ImageRunModal.jsx:1034
msgid ""
"Paste one or more lines of key=value pairs into any field for bulk import"
msgstr ""
"Toplu içe aktarma için herhangi bir alana bir veya daha fazla anahtar=değer "
"çifti satırı yapıştırın"

#: src/Containers.jsx:198 src/PodActions.jsx:138
msgid "Pause"
msgstr "Duraklat"

#: src/ContainerCommitModal.jsx:133
msgid "Pause container when creating image"
msgstr "Kalıp oluştururken kapsayıcıyı duraklatın"

#: src/util.js:8 src/util.js:11
msgid "Paused"
msgstr "Duraklatıldı"

#: src/ContainerDeleteModal.jsx:12
msgid "Please confirm deletion of $0"
msgstr "Lütfen $0 için silme işlemini onaylayın"

#: src/PodActions.jsx:170
msgid "Please confirm deletion of pod $0"
msgstr "Lütfen $0 bölmesinin silme işlemini onaylayın"

#: src/PodActions.jsx:170
msgid "Please confirm force deletion of pod $0"
msgstr "Lütfen $0 bölmesinin zorla silme işlemini onaylayın"

#: src/ForceRemoveModal.jsx:14
msgid "Please confirm forced deletion of $0"
msgstr "Lütfen $0 için zorla silme işlemini onaylayın"

#: src/ImageSearchModal.jsx:181
msgid "Please retry another term."
msgstr "Lütfen başka bir terim ile yeniden deneyin."

#: src/ImageSearchModal.jsx:176
msgid "Please start typing to look for images."
msgstr "Lütfen kalıpları aramak için yazmaya başlayın."

#: src/index.html:20 src/manifest.json:0
msgid "Podman containers"
msgstr "Podman kapsayıcıları"

#: src/app.jsx:583
msgid "Podman service is not active"
msgstr "Podman hizmeti etkin değil"

#: src/ImageRunModal.jsx:1011
msgid "Port mapping"
msgstr "Bağlantı noktası eşleme"

#: src/ContainerDetails.jsx:66 src/ImageDetails.jsx:39
msgid "Ports"
msgstr "Bağlantı noktaları"

#: src/ImageRunModal.jsx:198
msgid "Private"
msgstr "Özel"

#: src/ImageRunModal.jsx:102
msgid "Protocol"
msgstr "Protokol"

#: src/PruneUnusedImagesModal.jsx:101
msgid "Prune"
msgstr "Ayıkla"

#: src/Images.jsx:331 src/PruneUnusedImagesModal.jsx:94
msgid "Prune unused images"
msgstr "Kullanılmayan kalıpları ayıkla"

#: src/PruneUnusedImagesModal.jsx:97 src/PruneUnusedImagesModal.jsx:101
msgid "Pruning images"
msgstr "Kalıplar ayıklanıyor"

#: src/ImageRunModal.jsx:884
msgid "Pull latest image"
msgstr "Son kalıbı çek"

#: src/Images.jsx:303
msgid "Pulling"
msgstr "Çekiliyor"

#: src/ImageRunModal.jsx:117 src/ImageRunModal.jsx:165
#: src/ImageRunModal.jsx:205
msgid "Remove item"
msgstr "Öğeyi kaldır"

#: src/util.js:8
msgid "Removing"
msgstr "Kaldırılıyor"

#: src/Containers.jsx:186 src/PodActions.jsx:82
msgid "Restart"
msgstr "Yeniden başlat"

#: src/ImageRunModal.jsx:961
msgid "Restart policy"
msgstr "Yeniden başlatma ilkesi"

#: src/ImageRunModal.jsx:963 src/ImageRunModal.jsx:973
msgid "Restart policy help"
msgstr "Yeniden başlatma ilkesi yardımı"

#: src/ImageRunModal.jsx:965
msgid "Restart policy to follow when containers exit."
msgstr "Kapsayıcılardan çıkıldığında izlenecek ilkeyi yeniden başlatın."

#: src/Containers.jsx:233 src/ContainerRestoreModal.jsx:35
msgid "Restore"
msgstr "Geri yükle"

#: src/ContainerRestoreModal.jsx:30
msgid "Restore container $0"
msgstr "$0 kapsayıcısını geri yükle"

#: src/ContainerRestoreModal.jsx:46
msgid "Restore with established TCP connections"
msgstr "Kurulu TCP bağlantıları ile geri yükle"

#: src/Containers.jsx:205 src/PodActions.jsx:123
msgid "Resume"
msgstr "Sürdür"

#: src/util.js:8 src/util.js:11 src/ImageUsedBy.jsx:32
msgid "Running"
msgstr "Çalışıyor"

#: src/ImageRunModal.jsx:192
msgid "SELinux"
msgstr "SELinux"

#: src/ImageSearchModal.jsx:156
msgid "Search by name or description"
msgstr "Ada veya açıklamaya göre ara"

#: src/ImageRunModal.jsx:779
msgid "Search by registry"
msgstr "Kayıt defterine göre ara"

#: src/ImageSearchModal.jsx:153
msgid "Search for"
msgstr "Ara"

#: src/ImageSearchModal.jsx:214
msgid "Search for an image"
msgstr "Kalıp ara"

#: src/ImageRunModal.jsx:868
msgid "Search string or container location"
msgstr "Arama dizgisi veya kapsayıcı konumu"

#: src/ImageSearchModal.jsx:174
msgid "Searching..."
msgstr "Aranıyor..."

#: src/ImageRunModal.jsx:846
msgid "Searching: $0"
msgstr "Aranan: $0"

#: src/ImageRunModal.jsx:197
msgid "Shared"
msgstr "Paylaşılan"

#: src/Containers.jsx:530
msgid "Show"
msgstr "Göster"

#: src/Images.jsx:280
msgid "Show images"
msgstr "Kalıpları göster"

#: src/Images.jsx:235
msgid "Show intermediate images"
msgstr "Ara kalıpları göster"

#: src/PruneUnusedImagesModal.jsx:44
msgid "Show more"
msgstr "Daha fazlasını göster"

#: src/Containers.jsx:225 src/app.jsx:624 src/PodActions.jsx:108
msgid "Start"
msgstr "Başlat"

#: src/ImageRunModal.jsx:1002
msgid "Start after creation"
msgstr "Oluşturmadan sonra başlat"

#: src/app.jsx:590
msgid "Start podman"
msgstr "Podman'ı başlat"

#: src/Containers.jsx:429 src/ContainerDetails.jsx:88
msgid "State"
msgstr "Durum"

#: src/Containers.jsx:178 src/PodActions.jsx:60
msgid "Stop"
msgstr "Durdur"

#: src/util.js:8 src/util.js:11
msgid "Stopped"
msgstr "Durduruldu"

#: src/ContainerCheckpointModal.jsx:48
msgid "Support preserving established TCP connections"
msgstr "Kurulu TCP bağlantılarını korumayı destekle"

#: src/ContainerHeader.jsx:48 src/ImageRunModal.jsx:825
msgid "System"
msgstr "Sistem"

#: src/app.jsx:629
msgid "System Podman service is also available"
msgstr "Sistem Podman hizmeti de kullanılabilir"

#: src/ImageRunModal.jsx:108
msgid "TCP"
msgstr "TCP"

#: src/ContainerCommitModal.jsx:109 src/ImageSearchModal.jsx:218
msgid "Tag"
msgstr "Etiket"

#: src/ImageDetails.jsx:27
msgid "Tags"
msgstr "Etiketler"

#: src/app.jsx:595
msgid "Troubleshoot"
msgstr "Sorun Giderme"

#: src/ContainerHeader.jsx:56
msgid "Type to filter…"
msgstr "Süzmek için yazın…"

#: src/ImageRunModal.jsx:109
msgid "UDP"
msgstr "UDP"

#: src/ContainerDetails.jsx:11
msgid "Up since $0"
msgstr "$0'dan beri aktif"

#: src/ContainerCommitModal.jsx:138
msgid "Use legacy Docker format"
msgstr "Eski Docker biçimini kullan"

#: src/Images.jsx:166 src/ImageDetails.jsx:33
msgid "Used by"
msgstr "Kullanan"

#: src/app.jsx:66 src/app.jsx:512
msgid "User"
msgstr "Kullanıcı"

#: src/app.jsx:634
msgid "User Podman service is also available"
msgstr "Kullanıcı Podman hizmeti de kullanılabilir"

#: src/ImageRunModal.jsx:827
msgid "User:"
msgstr "Kullanıcı:"

#: src/ImageRunModal.jsx:155
msgid "Value"
msgstr "Değer"

#: src/ImageRunModal.jsx:1020
msgid "Volumes"
msgstr "Birimler"

#: src/ImageRunModal.jsx:898
msgid "With terminal"
msgstr "Terminal ile"

#: src/ImageRunModal.jsx:187
msgid "Writable"
msgstr "Yazılabilir"

#: src/manifest.json
msgid "container"
msgstr "kapsayıcı"

#: src/ImageRunModal.jsx:486
msgid "downloading"
msgstr "indiriliyor"

#: src/ImageRunModal.jsx:844
msgid "host[:port]/[user]/container[:tag]"
msgstr "anamakine[:b.noktası]/[kullanıcı]/kapsayıcı[:etiket]"

#: src/manifest.json
msgid "image"
msgstr "kalıp"

#: src/ImageSearchModal.jsx:161
msgid "in"
msgstr "şurada"

#: src/Containers.jsx:358 src/Containers.jsx:359
msgid "n/a"
msgstr "yok"

#: src/Containers.jsx:358 src/Containers.jsx:359
msgid "not available"
msgstr "kullanılabilir değil"

#: src/Containers.jsx:612
msgid "pod group"
msgstr "bölme grubu"

#: src/manifest.json
msgid "podman"
msgstr "podman"

#: src/ImageDeleteModal.jsx:80
msgid "select all"
msgstr "tümünü seç"

#: src/Containers.jsx:381 src/Images.jsx:123 src/ImageSearchModal.jsx:149
msgid "system"
msgstr "sistem"

#: src/Images.jsx:74 src/Images.jsx:81
msgid "unused"
msgstr "kullanılmayan"

#: src/Containers.jsx:381 src/Images.jsx:123
msgid "user:"
msgstr "kullanıcı:"

#~ msgid "Delete unused $0 images:"
#~ msgstr "Kullanılmayan $0 kalıplarını sil:"

#~ msgid "created"
#~ msgstr "oluşturuldu"

#~ msgid "exited"
#~ msgstr "çıkıldı"

#~ msgid "paused"
#~ msgstr "duraklatıldı"

#~ msgid "running"
#~ msgstr "çalışıyor"

#~ msgid "stopped"
#~ msgstr "durduruldu"

#~ msgid "user"
#~ msgstr "kullanıcı"

#~ msgid "Add on build variable"
#~ msgstr "Yapım değişkeninde ekle"

#~ msgid "Commit image"
#~ msgstr "Kalıbı işle"

#~ msgid "Format"
#~ msgstr "Biçim"

#~ msgid "Message"
#~ msgstr "İleti"

#~ msgid "Pause the container"
#~ msgstr "Konteyneri duraklat"

#~ msgid "Remove on build variable"
#~ msgstr "Yapım değişkeninde kaldır"

#~ msgid "Set container on build variables"
#~ msgstr "Yapım değişkenlerinde kapsayıcıyı ayarla"

#~ msgid "Add item"
#~ msgstr "Öğe ekle"

#~ msgid "Host port (optional)"
#~ msgstr "Anamakine bağlantı noktası (isteğe bağlı)"

#~ msgid "IP (optional)"
#~ msgstr "IP (isteğe bağlı)"

#~ msgid "ReadOnly"
#~ msgstr "SadeceOku"

#~ msgid "ReadWrite"
#~ msgstr "OkuYaz"

#~ msgid "IP prefix length"
#~ msgstr "IP ön ek uzunluğu"

#~ msgid "Get new image"
#~ msgstr "Yeni kalıp al"

#~ msgid "Run"
#~ msgstr "Çalıştır"

#~ msgid "Size"
#~ msgstr "Boyut"

#~ msgid "On build"
#~ msgstr "Yapım anında"

#~ msgid "Are you sure you want to delete this image?"
#~ msgstr "Bu kalıbı silmek istediğinizden emin misiniz?"

#~ msgid "Could not attach to this container: $0"
#~ msgstr "Bu konteynere iliştirilemedi: $0"

#~ msgid "Could not open channel: $0"
#~ msgstr "Kanal açılamadı: $0"

#~ msgid "Everything"
#~ msgstr "Her şey"

#~ msgid "Images and running containers"
#~ msgstr "Kalıplar ve çalışan konteynerler"

#~ msgid "Security"
#~ msgstr "Güvenlik"

#~ msgid "The scan from $time ($type) found no vulnerabilities."
#~ msgstr "$time ($type) taraması hiçbir güvenlik açığı bulamadı."

#~ msgid "This version of the Web Console does not support a terminal."
#~ msgstr "Web Konsolu'nun bu sürümü bir terminali desteklememektedir."
